import os
from flask import Flask, jsonify, request
from flask_cors import CORS

import json
from content_based import get_recommendations_desc
from trending_movies import *

HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}



def flask_app():
    app = Flask(__name__)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

    @app.route('/', methods=['GET'])
    def server_is_up():
        # print("success")
        return 'server is up'

    @app.route('/content_based', methods=['POST'])
    def content_based():
        content = request.json
        pred = get_recommendations_desc(content["title"])
        return jsonify({'data':pred})  
    
    @app.route('/trending_movies', methods=['GET'])
    def trending_movies():
        trends = custom_df["title"].head(10).tolist()
        return jsonify({'data':trends}) 
    return app

if __name__ == '__main__':
    app = flask_app()
    app.run(debug=True, host='0.0.0.0')


