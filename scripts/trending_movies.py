from datetime import datetime
import pandas as pd

def str_to_date(str):
    try:
        return datetime.strptime(str, '%Y-%m-%d').date()
    except:
        return datetime.strptime("0001-01-01", '%Y-%m-%d').date()

def str_to_int(str):
    try:
        return int(str)
    except:
        return 0
custom_df = pd.read_csv("movies_metadata.csv")

custom_df.release_date=custom_df.release_date.dropna()
custom_df.release_date = custom_df.release_date.apply(lambda x: str(x))
custom_df.popularity = custom_df.popularity.apply(lambda x: str_to_int(x))

custom_df.release_date = custom_df.release_date.apply(lambda x: str_to_date(x))

custom_df = custom_df.sort_values(['release_date'], ascending=False)[custom_df["popularity"]!=0].iloc[1:50]
custom_df = custom_df.sort_values(['popularity'], ascending=False)

