from ast import literal_eval
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import pandas as pd
import pickle

print("Importing Data .. ")
sdf = pd.read_csv("sdf.csv")

print("Loading Pickle .. ")
infile = open("tfidf.pkl",'rb')
tfidf_matrix = pickle.load(infile)
infile.close()

print("Creating cosine similarity Matrix .. ")
cosine_sim = linear_kernel(tfidf_matrix, tfidf_matrix)

indices = pd.Series(sdf.index, index=sdf['title']).drop_duplicates()

#Get recommendation function
def get_recommendations_desc (title):
    idx = indices[title]
    sim_scores = list(enumerate(cosine_sim[idx]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    sim_scores = sim_scores[1:10]
    movie_indices = [i[0] for i in sim_scores]
    return sdf['title'].iloc[movie_indices].tolist()
preds = get_recommendations_desc('Sense and Sensibility')
print(preds)
#print()